package com.daimajia.numberprogressbar;


import com.daimajia.numberprogressbar.utils.RectF;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;


/**
 * Created by daimajia on 14-4-30.
 */
public class NumberProgressBar extends Component implements Component.DrawTask {

    private int mMaxProgress = 100;

    /**
     * Current progress, can not exceed the max progress.
     */
    private int mCurrentProgress = 0;

    /**
     * The progress area bar color.
     */
    private Color mReachedBarColor = Color.BLUE;

    /**
     * The bar unreached area color.
     */
    private Color mUnreachedBarColor = Color.LTGRAY;

    /**
     * The progress text color.
     */
    private Color mTextColor = Color.BLACK;

    /**
     * The progress text size.
     */
    private int mTextSize=sp2px(10);

    /**
     * The height of the reached area.
     */
    private float mReachedBarHeight= dp2px(1.5f);

    /**
     * The height of the unreached area.
     */
    private float mUnreachedBarHeight= dp2px(1);

    /**
     * The suffix of the number.
     */
    private String mSuffix = "%";

    /**
     * The prefix.
     */
    private String mPrefix = "";

    private static final int PROGRESS_TEXT_VISIBLE = 0;


    /**
     * The width of the text that to be drawn.
     */
    private float mDrawTextWidth;

    /**
     * The drawn text start.
     */
    private float mDrawTextStart;

    /**
     * The drawn text end.
     */
    private float mDrawTextEnd;

    /**
     * The text that to be drawn in onDraw().
     */
    private String mCurrentDrawText;

    /**
     * The Paint of the reached area.
     */
    private Paint mReachedBarPaint;
    /**
     * The Paint of the unreached area.
     */
    private Paint mUnreachedBarPaint;
    /**
     * The Paint of the progress text.
     */
    private Paint mTextPaint;

    /**
     * Unreached bar area to draw rect.
     */
    private RectF mUnreachedRectF = new RectF();
    /**
     * Reached bar area rect.
     */
    private RectF mReachedRectF = new RectF();

    /**
     * The progress text offset.
     */
    private float mOffset = 3;

    /**
     * Determine if need to draw unreached area.
     */
    private boolean mDrawUnreachedBar = true;

    private boolean mDrawReachedBar = true;

    private boolean mIfDrawText = true;

    private static final String REACHED_COLOR = "progress_reached_color";
    private static final String UNREACHED_COLOR = "progress_unreached_color";
    private static final String TEXT_COLOR = "progress_text_color";
    private static final String TEXT_SIZE = "progress_text_size";
    private static final String REACHED_BAR_HEIGHT = "progress_reached_bar_height";
    private static final String UNREACHED_BAR_HEIGHT = "progress_unreached_bar_height";
    private static final String TEXT_OFFSET = "progress_text_offset";
    private static final String TEXT_VISIBILITY = "progress_text_visibility";
    private static final String PROGRESS_CURRENT = "progress_current";
    private static final String PROGRESS_MAX = "progress_max";

    /**
     * Listener
     */
    private OnProgressBarListener mListener;

    public enum ProgressTextVisibility {
        Visible, Invisible
    }

    public NumberProgressBar(Context context) {
        this(context, null);
    }

    public NumberProgressBar(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    public NumberProgressBar(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        boolean presentReachedBarColor = attrs.getAttr(REACHED_COLOR).isPresent();
        if (presentReachedBarColor) {
            mReachedBarColor = attrs.getAttr(REACHED_COLOR).get().getColorValue();
        }
        boolean presentUnreachedBarColor = attrs.getAttr(UNREACHED_COLOR).isPresent();
        if (presentUnreachedBarColor) {
            mUnreachedBarColor = attrs.getAttr(UNREACHED_COLOR).get().getColorValue();
        }
        boolean presentTextColor = attrs.getAttr(TEXT_COLOR).isPresent();
        if (presentTextColor) {
            mTextColor = attrs.getAttr(TEXT_COLOR).get().getColorValue();
        }
        boolean presentTextSize = attrs.getAttr(TEXT_SIZE).isPresent();
        if (presentTextSize) {
            mTextSize = attrs.getAttr(TEXT_SIZE).get().getDimensionValue();
        }
        boolean presentReachedBarHeight = attrs.getAttr(REACHED_BAR_HEIGHT).isPresent();
        if (presentReachedBarHeight) {
            mReachedBarHeight = attrs.getAttr(REACHED_BAR_HEIGHT).get().getDimensionValue();
        }
        boolean presentUnreachedBarHeight = attrs.getAttr(UNREACHED_BAR_HEIGHT).isPresent();
        if (presentUnreachedBarHeight) {
            mUnreachedBarHeight = attrs.getAttr(UNREACHED_BAR_HEIGHT).get().getDimensionValue();
        }
        boolean presentOffset = attrs.getAttr(TEXT_OFFSET).isPresent();
        if (presentOffset) {
            mOffset = attrs.getAttr(TEXT_OFFSET).get().getDimensionValue();
        }
        boolean presentTextVisible = attrs.getAttr(TEXT_VISIBILITY).isPresent();
        if (presentTextVisible) {
            int textVisible = attrs.getAttr(TEXT_VISIBILITY).get().getIntegerValue();
            mIfDrawText = textVisible == PROGRESS_TEXT_VISIBLE;
        }
        boolean presentProgressCurrent = attrs.getAttr(PROGRESS_CURRENT).isPresent();
        if (presentProgressCurrent) {
            int currentProgress = attrs.getAttr(PROGRESS_CURRENT).get().getIntegerValue();
            setProgress(currentProgress);
        }
        boolean presentProgressMax = attrs.getAttr(PROGRESS_MAX).isPresent();
        if (presentProgressMax) {
            int maxProgress = attrs.getAttr(PROGRESS_MAX).get().getIntegerValue();
            setMax(maxProgress);
        }

        initializePainters();
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (mIfDrawText) {
            calculateDrawRectF();
        } else {
            calculateDrawRectFWithoutProgressText();
        }

        if (mDrawReachedBar) {
            canvas.drawRect(mReachedRectF, mReachedBarPaint);
        }

        if (mDrawUnreachedBar) {
            canvas.drawRect(mUnreachedRectF, mUnreachedBarPaint);
        }

        if (mIfDrawText){
            canvas.drawText(mTextPaint, mCurrentDrawText, mDrawTextStart, mDrawTextEnd);
        }
    }

    private void initializePainters() {
        mReachedBarPaint = new Paint();
        mReachedBarPaint.setAntiAlias(true);
        mReachedBarPaint.setColor(mReachedBarColor);

        mUnreachedBarPaint = new Paint();
        mUnreachedBarPaint.setAntiAlias(true);
        mUnreachedBarPaint.setColor(mUnreachedBarColor);

        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setColor(mTextColor);
        mTextPaint.setTextSize(mTextSize);
    }

    private void calculateDrawRectFWithoutProgressText() {
        mReachedRectF.left = getPaddingLeft();
        mReachedRectF.top = getHeight() / 2.0f - mReachedBarHeight / 2.0f;
        mReachedRectF.right = (getWidth() - getPaddingLeft() - getPaddingRight()) / (getMax() * 1.0f) * getProgress() + getPaddingLeft();
        mReachedRectF.bottom = getHeight() / 2.0f + mReachedBarHeight / 2.0f;

        mUnreachedRectF.left = mReachedRectF.right;
        mUnreachedRectF.right = getWidth() - getPaddingRight();
        mUnreachedRectF.top = getHeight() / 2.0f + -mUnreachedBarHeight / 2.0f;
        mUnreachedRectF.bottom = getHeight() / 2.0f + mUnreachedBarHeight / 2.0f;
    }

    private void calculateDrawRectF() {

        mCurrentDrawText = String.format("%d", getProgress() * 100 / getMax());
        mCurrentDrawText = mPrefix + mCurrentDrawText + mSuffix;
        mDrawTextWidth = mTextPaint.measureText(mCurrentDrawText);

        if (getProgress() == 0) {
            mDrawReachedBar = false;
            mDrawTextStart = getPaddingLeft();
        } else {
            mDrawReachedBar = true;
            mReachedRectF.left = getPaddingLeft();
            mReachedRectF.top = getHeight() / 2.0f - mReachedBarHeight / 2.0f;
            mReachedRectF.right = (getWidth() - getPaddingLeft() - getPaddingRight()) / (getMax() * 1.0f) * getProgress() - mOffset + getPaddingLeft();
            mReachedRectF.bottom = getHeight() / 2.0f + mReachedBarHeight / 2.0f;
            mDrawTextStart = (mReachedRectF.right + mOffset);
        }

        mDrawTextEnd = (int) ((getHeight() / 2.0f) - ((mTextPaint.getFontMetrics().descent + mTextPaint.getFontMetrics().ascent) / 2.0f));

        if ((mDrawTextStart + mDrawTextWidth) >= getWidth() - getPaddingRight()) {
            mDrawTextStart = getWidth() - getPaddingRight() - mDrawTextWidth;
            mReachedRectF.right = mDrawTextStart - mOffset;
        }

        float unreachedBarStart = mDrawTextStart + mDrawTextWidth + mOffset;
        if (unreachedBarStart >= getWidth() - getPaddingRight()) {
            mDrawUnreachedBar = false;
        } else {
            mDrawUnreachedBar = true;
            mUnreachedRectF.left = unreachedBarStart;
            mUnreachedRectF.right = getWidth() - getPaddingRight();
            mUnreachedRectF.top = getHeight() / 2.0f + -mUnreachedBarHeight / 2.0f;
            mUnreachedRectF.bottom = getHeight() / 2.0f + mUnreachedBarHeight / 2.0f;
        }
    }

    /**
     * Get progress text color.
     *
     * @return progress text color.
     */
    public Color getTextColor() {
        return mTextColor;
    }

    /**
     * Get progress text size.
     *
     * @return progress text size.
     */
    public int getProgressTextSize() {
        return mTextSize;
    }

    public Color getUnreachedBarColor() {
        return mUnreachedBarColor;
    }

    public Color getReachedBarColor() {
        return mReachedBarColor;
    }

    public int getProgress() {
        return mCurrentProgress;
    }

    public int getMax() {
        return mMaxProgress;
    }

    public float getReachedBarHeight() {
        return mReachedBarHeight;
    }

    public float getUnreachedBarHeight() {
        return mUnreachedBarHeight;
    }

    public void setProgressTextSize(int textSize) {
        this.mTextSize = textSize;
        mTextPaint.setTextSize(mTextSize);
        invalidate();
    }

    public void setProgressTextColor(Color textColor) {
        this.mTextColor = textColor;
        mTextPaint.setColor(mTextColor);
        invalidate();
    }

    public void setUnreachedBarColor(Color barColor) {
        this.mUnreachedBarColor = barColor;
        mUnreachedBarPaint.setColor(mUnreachedBarColor);
        invalidate();
    }

    public void setReachedBarColor(Color progressColor) {
        this.mReachedBarColor = progressColor;
        mReachedBarPaint.setColor(mReachedBarColor);
        invalidate();
    }

    public void setReachedBarHeight(float height) {
        mReachedBarHeight = height;
    }

    public void setUnreachedBarHeight(float height) {
        mUnreachedBarHeight = height;
    }

    public void setMax(int maxProgress) {
        if (maxProgress > 0) {
            this.mMaxProgress = maxProgress;
            invalidate();
        }
    }

    public void setSuffix(String suffix) {
        if (suffix == null) {
            mSuffix = "";
        } else {
            mSuffix = suffix;
        }
    }

    public String getSuffix() {
        return mSuffix;
    }

    public void setPrefix(String prefix) {
        if (prefix == null)
            mPrefix = "";
        else {
            mPrefix = prefix;
        }
    }

    public String getPrefix() {
        return mPrefix;
    }

    public void incrementProgressBy(int by) {
        if (by > 0) {
            setProgress(getProgress() + by);
        }

        if (mListener != null) {
            mListener.onProgressChange(getProgress(), getMax());
        }
    }

    public void setProgress(int progress) {
        if (progress <= getMax() && progress >= 0) {
            this.mCurrentProgress = progress;
            invalidate();
        }
    }

    private final static float NORMAL_PIXEL_DENSITY = 160f;

    private int dp2px(float dp) {
        float scale = getResourceManager().getDeviceCapability().screenDensity;
        return (int) (dp * scale / NORMAL_PIXEL_DENSITY + 0.5f);
    }

    private int sp2px(float sp) {
        float scale = getResourceManager().getDeviceCapability().screenDensity;
        return (int) (sp * scale / NORMAL_PIXEL_DENSITY + 0.5f);
    }

    public void setProgressTextVisibility(ProgressTextVisibility visibility) {
        mIfDrawText = visibility == ProgressTextVisibility.Visible;
        invalidate();
    }

    public boolean getProgressTextVisibility() {
        return mIfDrawText;
    }

    public void setOnProgressBarListener(OnProgressBarListener listener) {
        mListener = listener;
    }
}