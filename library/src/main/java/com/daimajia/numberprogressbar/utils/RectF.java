/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.daimajia.numberprogressbar.utils;

import ohos.agp.utils.RectFloat;

/**
 *  Rect工具
 */
public class RectF extends RectFloat {
    @Override
    public void modify(float left, float top, float right, float bottom) {
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
    }

    /**
     * 设置RectF
     * @param left 左
     * @param top 上
     * @param right 右
     * @param bottom 下
     */
    public void set(float left, float top, float right, float bottom) {
        modify(left,top,right,bottom);
    }
}
