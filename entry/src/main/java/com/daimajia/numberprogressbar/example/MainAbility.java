/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.daimajia.numberprogressbar.example;

import com.daimajia.numberprogressbar.NumberProgressBar;
import com.daimajia.numberprogressbar.OnProgressBarListener;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.window.dialog.ToastDialog;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 演示效果
 */
public class MainAbility extends Ability implements OnProgressBarListener {

    private Timer timer;
    private NumberProgressBar progressBar1;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        progressBar1 = (NumberProgressBar) findComponentById(ResourceTable.Id_npb1);
        progressBar1.setOnProgressBarListener(this);
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                getUITaskDispatcher().asyncDispatch(() -> progressBar1.incrementProgressBy(1));
            }
        }, 500, 100);

        NumberProgressBar progressBar2 = (NumberProgressBar) findComponentById(ResourceTable.Id_npb2);
        progressBar2.setProgress(60);
    }

    @Override
    public void onProgressChange(int current, int max) {
        if(current == max) {
            new ToastDialog(this).setText("ProgressBar is finished.").show();
            progressBar1.setProgress(0);
        }
    }

    @Override
    protected void onStop() {
        timer.cancel();
        timer = null;
        super.onStop();
    }

}
