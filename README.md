# NumberProgressBar

NumberProgressBar is a combination of various types of progressBar.

### Demo
![NumberProgressBar](https://gitee.com/openharmony-tpc/NumberProgressBar/raw/master/screenshot/demo.gif)

### Usage
----

Solution 1: local har package integration
1. Add the har package to the lib folder.
2. Add the following code to gradle of the entry:
```groovy
implementation fileTree(dir: 'libs', include: ['*.jar','*.har'])
```
Solution 2:
```groovy
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:NumberProgressBar:1.0.1'
```

Use it in your own code:

```xml
	<com.daimajia.numberprogressbar.NumberProgressBar
		ohos:id="$+id:number_progress_bar"
		ohos:width="match_parent"
		ohos:height="match_content"
	/>
```

## Entry Running Requirements
1. Use DevEco Studio and download the SDK.
2. Change the dependencies→classpath version in the build.gradle file to the corresponding version. (that is, the version used in your IDE's new project)

### Attributes

There are several attributes you can set:


The **reached area** and **unreached area**:

* color
* height

The **text area**:

* color
* text size
* visibility
* distance between **reached area** and **unreached area**

The **bar**:

* max progress
* current progress

for example:

```xml
	<com.daimajia.numberprogressbar.NumberProgressBar
	        ohos:width="match_parent"
	        ohos:height="10vp"
	        app:progress_unreached_color="#CCCCCC"
	        app:progress_reached_color="#3498DB"
	        app:progress_unreached_bar_height="1vp"
	        app:progress_reached_bar_height="2vp"
	        app:progress_text_size="10fp"
	        app:progress_text_color="#3498DB"
	        app:progress_text_offset="1vp"
	        app:progress_text_visibility="1"
	        app:progress_max="100"
	        app:progress_current="80"/>
```